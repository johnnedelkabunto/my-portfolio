const checkbox = document.getElementById('checkbox');
var navMode = document.getElementById('mynav');
var navLink = document.getElementById('link');
var icon = document.getElementById('toggle-icon');
var footer = document.getElementById('footer');
var projectCard = document.getElementById('project-card');
var projectCard1 = document.getElementById('project-card1');
var projectCard2 = document.getElementById('project-card2');
var projectCard3 = document.getElementById('project-card3');
var projectCard4 = document.getElementById('project-card4');

checkbox.addEventListener('change', () => {

    document.body.classList.toggle('body-dark');
    navMode.classList.toggle('nav-dark');

    
    
    // navbar modes
    if (navMode.classList.contains('navbar-light')){
        navMode.classList.remove('navbar-light');
        navMode.classList.add('navbar-dark');
    } else {
        navMode.classList.add('navbar-light');
        navMode.classList.remove('navbar-dark');
    }

    // toggle icon
    if (icon.classList.contains('ion-ios-sunny')){
        icon.classList.remove('ion-ios-sunny');
        icon.classList.add('ion-ios-moon');
    } else {
        icon.classList.add('ion-ios-sunny');
        icon.classList.remove('ion-ios-moon');
    }

    // footer modes
    if (footer.classList.contains('footer-light')){
        footer.classList.remove('footer-light');
        footer.classList.add('footer-dark');
    } else {
        footer.classList.add('footer-light');
        footer.classList.remove('footer-dark');
    }

    // project modes
    if (projectCard.classList.contains('projects-card-dark')){
        projectCard.classList.remove('projects-card-dark');
        projectCard.classList.add('projects-card-light');
    } else {
        projectCard.classList.add('projects-card-dark');
        projectCard.classList.remove('projects-card-light');
    } 

    if (projectCard1.classList.contains('projects-card-dark')){
        projectCard1.classList.remove('projects-card-dark');
        projectCard1.classList.add('projects-card-light');
    } else {
        projectCard1.classList.add('projects-card-dark');
        projectCard1.classList.remove('projects-card-light');
    }

    if (projectCard2.classList.contains('projects-card-dark')){
        projectCard2.classList.remove('projects-card-dark');
        projectCard2.classList.add('projects-card-light');
    } else {
        projectCard2.classList.add('projects-card-dark');
        projectCard2.classList.remove('projects-card-light');
    }

    if (projectCard3.classList.contains('projects-card-dark')){
        projectCard3.classList.remove('projects-card-dark');
        projectCard3.classList.add('projects-card-light');
    } else {
        projectCard3.classList.add('projects-card-dark');
        projectCard3.classList.remove('projects-card-light');
    }

    if (projectCard4.classList.contains('projects-card-dark')){
        projectCard4.classList.remove('projects-card-dark');
        projectCard4.classList.add('projects-card-light');
    } else {
        projectCard4.classList.add('projects-card-dark');
        projectCard4.classList.remove('projects-card-light');
    }
   
});


// icon hover animation

var html = document.getElementById('html');
var css = document.getElementById('css');
var js = document.getElementById('js');
var php = document.getElementById('php');
var git = document.getElementById('git');
var laravel = document.getElementById('laravel');
var mySql = document.getElementById('mySql');
var gitLab = document.getElementById('gitLab');
var jq = document.getElementById('jq');
var node = document.getElementById('node');
var ex = document.getElementById('ex');
var react = document.getElementById('react');
var mdb = document.getElementById('mdb');

// html
html.addEventListener('mouseenter', () => {
    html.classList.add('colored');
})

html.addEventListener('mouseleave', () => {
    html.classList.remove('colored');
})

// css
css.addEventListener('mouseenter', () => {
    css.classList.add('colored');
})

css.addEventListener('mouseleave', () => {
    css.classList.remove('colored');
})

// css
js.addEventListener('mouseenter', () => {
    js.classList.add('colored');
})

js.addEventListener('mouseleave', () => {
    js.classList.remove('colored');
})

// php
php.addEventListener('mouseenter', () => {
    php.classList.add('colored');
})

php.addEventListener('mouseleave', () => {
    php.classList.remove('colored');
})

// git
git.addEventListener('mouseenter', () => {
    git.classList.add('colored');
})

git.addEventListener('mouseleave', () => {
    git.classList.remove('colored');
})

// laravel
laravel.addEventListener('mouseenter', () => {
    laravel.classList.add('colored');
})

laravel.addEventListener('mouseleave', () => {
    laravel.classList.remove('colored');
})

// mySql
mySql.addEventListener('mouseenter', () => {
    mySql.classList.add('colored');
})

mySql.addEventListener('mouseleave', () => {
    mySql.classList.remove('colored');
})

// gitLab
gitLab.addEventListener('mouseenter', () => {
    gitLab.classList.add('colored');
})

gitLab.addEventListener('mouseleave', () => {
    gitLab.classList.remove('colored');
})

// jq
jq.addEventListener('mouseenter', () => {
    jq.classList.add('colored');
})

jq.addEventListener('mouseleave', () => {
    jq.classList.remove('colored');
})

// node
node.addEventListener('mouseenter', () => {
    node.classList.add('colored');
})

node.addEventListener('mouseleave', () => {
    node.classList.remove('colored');
})

// ex
ex.addEventListener('mouseenter', () => {
    ex.classList.add('colored');
})

ex.addEventListener('mouseleave', () => {
    ex.classList.remove('colored');
})

// react
react.addEventListener('mouseenter', () => {
    react.classList.add('colored');
})

react.addEventListener('mouseleave', () => {
    react.classList.remove('colored');
})

// mdb
mdb.addEventListener('mouseenter', () => {
    mdb.classList.add('colored');
})

mdb.addEventListener('mouseleave', () => {
    mdb.classList.remove('colored');
})










